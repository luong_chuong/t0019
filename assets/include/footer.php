<footer class="c-footer">
	<div class="c-footer__1">
		<img src="/assets/image/common/logo.png" alt="" width="480" height="90" class="u-pc-only">
		<img src="/assets/image/common/sp/logo-ft.png" alt="" width="240" height="50" class="u-sp-only">
		<div class="c-footer__text">
			<p>〒355-0028 埼玉県東松山市箭弓町1-13-16 安福ビル1F　TEL.0493-23-8015</p>
			<p>営業時間：［平日］11:00〜21:00（昼休憩13:00〜14:00）／［土・日・祝］10:00〜19:00　火曜定休</p>
			<p class="u-pc-only">レッスン・フリー利用可能時間：会員・ビジター問わず1時間（準備時間含む）</p>
		</div>
	</div>
	<div class="c-footer__2">
		<p>Copyright (c) Shin Corporation All Rights Reserved.</p>
	</div>
	<div class="c-footer__btn">
		<a>
			<img src="assets/image/common/button_backtop.png" alt="" width="100" height="100" class="u-pc-only">
			<img src="assets/image/common/sp/button_backtop.png" alt="" width="50" height="50" class="u-sp-only">
		</a>
	</div>
</footer>

<script src="/assets/js/functions.js"></script>
</body>
</html>