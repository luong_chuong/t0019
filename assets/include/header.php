<!DOCTYPE html>
<html lang="ja" id="pagetop">
<head>
<meta charset="UTF-8">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/meta.php'); ?>
<link href="/assets/css/style.css" rel="stylesheet">
<script src="/assets/js/jquery-2.2.4.min.js"></script>
</head>
<body class="page-<?php echo $id; ?>">
	<header class="c-header">
		<img src="assets/image/common/sp/banner.png" alt="" width="320" class="u-sp-only">
		<div class="c-header__inner">
			<img src="assets/image/common/banner.png" alt="" class="u-pc-only">
			<img src="assets/image/common/sp/logo.png" alt="" class="u-sp-only" width="280" height="123">
		</div>
	</header>
	<nav class="c-gnavi">
		<ul>
			<li><a href="">キャンペーン情報</a></li>
			<li><a href="">ご利用料金</a></li>
			<li><a href="">ご予約</a></li>
			<li><a href="">講師紹介</a></li>
			<li><a href="">店舗のご案内</a></li>
		</ul>
	</nav>