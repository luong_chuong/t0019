<?php $id="index";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>
<!--begin section 1-->
<section class="p-index1">
	<div class="p-index1__title">
		<h3>
			<img src="assets/image/common/title_1.png" alt="title" width="656" height="920" class="u-pc-only">
			<img src="assets/image/common/sp/title_1.png" alt="title" width="320" class="u-sp-only">
		</h3>
	</div>
	<div class="p-index1__content">
		<img src="assets/image/common/1.png" alt="" width="480" height="402" class="pc">
		<img src="assets/image/common/2.png" alt="" width="484" height="416" class="pc">
		<img src="assets/image/common/3.png" alt="" width="480" height="402" class="pc">
		<img src="assets/image/common/sp/1.png" alt="" width="304" class="u-mgAuto u-sp-only">
		<div class="p-index1__contentSP">
			<img src="assets/image/common/sp/2.png" alt="" width="151" height="211" class="u-sp-only">
			<img src="assets/image/common/sp/3.png" alt="" width="151" height="211" class="u-sp-only">
		</div>
		<div class="c-btn">
			<a href="">見学・体験レッスンのご予約はこちら</a>
		</div>
	</div>
</section>
<!--begin section 2-->
<section class="p-index2">
	<div class="p-index2__inner">
		<div class="p-index2__title">
			<h2>
				<img src="assets/image/common/title_fee.png" alt="Title" width="379" height="68" class="u-pc-only u-mgAuto">
				<img src="assets/image/common/sp/title_fee.png" alt="Title" width="190" height="34" class="u-sp-only u-mgAuto">
			</h2>
		</div>
		<div class="p-index2__content">
			<div class="c-table" id="table1">
				<table>
					<caption>事前申し込み＜期間限定＞</caption>
					<tr>
						<td>
							<div class="c-discount">
								<span class="c-tag">体験レッスン</span>
								<span class="c-price u-del"><i>¥2,000</i> (税抜)</span>
								<i class="c-finalPrice">¥0</i>
							</div>
							<div class="c-discount">
								<span class="c-tag">フリー練習</span>
								<span class="c-price u-del"><i>¥1,500</i> (税抜)</span>
								<i class="c-finalPrice">¥0</i>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="p-index2__content">
			<div class="c-table" id="table2">
				<table>
					<caption>通常料金 ※(税抜)</caption>
					<tr>
						<th></th>
						<th></th>
						<th>会員</th>
						<th>ビジター</th>
						<th>フリー練習費</th>
					</tr>
					<tr>
						<td>⼊会⾦</td>
						<td></td>
						<td>￥5,000／1回</td>
						<td>−</td>
						<td>−</td>
					</tr>
					<tr>
						<td>レギュラー</td>
						<td>全⽇利⽤可</td>
						<td>￥10,000（税抜）／⽉額</td>
						<td>−</td>
						<td><span class="u-redbold">無料</span></td>
					</tr>
					<tr>
						<td>デイタイム</td>
						<td>平⽇昼限定（11:00〜17:00）</td>
						<td>￥7,000（税抜）／⽉額</td>
						<td>−</td>
						<td>時間外有料</td>
					</tr>
					<tr>
						<td>ジュニア</td>
						<td><p>⽊曜⽇限定 17：00〜18：00</p>
							<p>中学3年生まで</p></td>
						<td>￥6,000（税抜）／⽉額</td>
						<td>−</td>
						<td>時間外有料</td>
					</tr>
					<tr>
						<td>ホリデイ</td>
						<td>⼟・⽇・祝のみ終⽇利⽤可</td>
						<td>￥8,000（税抜）／⽉額</td>
						<td>−</td>
						<td>時間外有料</td>
					</tr>
					<tr>
						<td>レッスン4</td>
						<td><p>レッスン4回</p>
							<p>（スタンプカード制／有効期間：3ヶ月</p></td>
						<td>−</td>
						<td>￥8,000（税抜）／1セット</td>
						<td>有料</td>
					</tr>
					<tr>
						<td>フリー練習</td>
						<td><p>空打席があれば利⽤可</p>
							<p>※レッスンは含まず</p></td>
						<td>
							<p><span class="u-del">￥1,000（税抜）／1回</span></p>
							<p><span class="u-redbold">プレオープン期間限定 ￥0</span></p>
						</td>
						<td>
							<p><span class="u-del">￥1,500（税抜）／1回</span></p>
							<p><span class="u-redbold">プレオープン期間限定 ￥0</span></p>
						</td>
						<td>有料</td>
					</tr>
					<tr>
						<td>体験レッスン</td>
						<td>全⽇利⽤可</td>
						<td>−</td>
						<td>￥2,000（税抜）／1回</td>
						<td>−</td>
					</tr>
				</table>
			</div>
			<div class="c-accordion" id="">
				<div class="c-accordion__caption">通常料金 ※(税抜)</div>
				<!-- content -->
				<button class="c-accordion__btn"><b>入会金</b></button>
				<div class="c-accordion__panel">
					<div class="c-accordion__row">
						<div class="c-accordion__panel-left">⼊会⾦</div>
						<div class="c-accordion__panel-right">￥5,000（税抜）／1回</div>
					</div>
				</div>
				<!-- content -->
				<button class="c-accordion__btn"><b>レギュラー</b>全⽇利⽤可</button>
				<div class="c-accordion__panel">
					<div class="c-accordion__row">
						<div class="c-accordion__panel-left">会員</div>
						<div class="c-accordion__panel-right">￥10,000（税抜）／⽉額</div>
					</div>
					<div class="c-accordion__row">
						<div class="c-accordion__panel-left">ビジター</div>
						<div class="c-accordion__panel-right">-</div>
					</div>
					<div class="c-accordion__row">
						<div class="c-accordion__panel-left">フリー練習費</div>
						<div class="c-accordion__panel-right">無料</div>
					</div>
					<div class="c-accordion__row">
						<div class="c-accordion__panel-left">打席料・ボール代</div>
						<div class="c-accordion__panel-right">無料</div>
					</div>
				</div>
				<!-- content -->
				<button class="c-accordion__btn"><b>デイタイム</b>平⽇昼限定（11:00〜17:00）</button>
				<div class="c-accordion__panel">
					<div class="c-accordion__row">
						<div class="c-accordion__panel-left">会員</div>
						<div class="c-accordion__panel-right">￥7,000（税抜）／⽉額</div>
					</div>
					<div class="c-accordion__row">
						<div class="c-accordion__panel-left">ビジター</div>
						<div class="c-accordion__panel-right">-</div>
					</div>
					<div class="c-accordion__row">
						<div class="c-accordion__panel-left">フリー練習費</div>
						<div class="c-accordion__panel-right">時間外有料</div>
					</div>
				</div>
				<!-- content -->
				<button class="c-accordion__btn"><b>ジュニア</b>⽊曜⽇限定 17：00〜18：00<br>中学3年生まで</button>
				<div class="c-accordion__panel">
					<div class="c-accordion__row">
						<div class="c-accordion__panel-left">会員</div>
						<div class="c-accordion__panel-right">￥6,000（税抜）／⽉額</div>
					</div>
					<div class="c-accordion__row">
						<div class="c-accordion__panel-left">ビジター</div>
						<div class="c-accordion__panel-right">-</div>
					</div>
					<div class="c-accordion__row">
						<div class="c-accordion__panel-left">フリー練習費</div>
						<div class="c-accordion__panel-right">無料</div>
					</div>
				</div>
				<!-- content -->
				<button class="c-accordion__btn"><b>ホリデイ</b>⼟・⽇・祝のみ終⽇利⽤可</button>
				<div class="c-accordion__panel">
					<div class="c-accordion__row">
						<div class="c-accordion__panel-left">会員</div>
						<div class="c-accordion__panel-right">￥8,000（税抜）／⽉額</div>
					</div>
					<div class="c-accordion__row">
						<div class="c-accordion__panel-left">ビジター</div>
						<div class="c-accordion__panel-right">-</div>
					</div>
					<div class="c-accordion__row">
						<div class="c-accordion__panel-left">フリー練習費</div>
						<div class="c-accordion__panel-right">時間外有料</div>
					</div>
				</div>
				<!-- content -->
				<button class="c-accordion__btn"><b>レッスン4</b>レッスン4回（スタンプカード制<br>／有効期間：3ヶ月）</button>
				<div class="c-accordion__panel">
					<div class="c-accordion__row">
						<div class="c-accordion__panel-left">会員</div>
						<div class="c-accordion__panel-right">-</div>
					</div>
					<div class="c-accordion__row">
						<div class="c-accordion__panel-left">ビジター</div>
						<div class="c-accordion__panel-right">￥8,000（税抜）／⽉額</div>
					</div>
					<div class="c-accordion__row">
						<div class="c-accordion__panel-left">フリー練習費</div>
						<div class="c-accordion__panel-right">無料</div>
					</div>
				</div>
				<!-- content -->
				<button class="c-accordion__btn"><b>フリー練習</b>空打席があれば利⽤可<br>※レッスンは含まず</button>
				<div class="c-accordion__panel">
					<div class="c-accordion__row">
						<div class="c-accordion__panel-left">会員</div>
						<div class="c-accordion__panel-right">
							<p><span class="u-del">￥1,000（税抜）／1回</span></p>
							<p><span class="u-redbold">プレオープン期間限定 ￥0</span></p>
						</div>
					</div>
					<div class="c-accordion__row">
						<div class="c-accordion__panel-left">ビジター</div>
						<div class="c-accordion__panel-right">
							<p><span class="u-del">￥1,500（税抜）／1回</span></p>
							<p><span class="u-redbold">プレオープン期間限定 ￥0</span></p>
						</div>
					</div>
					<div class="c-accordion__row">
						<div class="c-accordion__panel-left">フリー練習費</div>
						<div class="c-accordion__panel-right">無料</div>
					</div>
				</div>
				<!-- content -->
				<button class="c-accordion__btn"><b>体験レッスン</b>全⽇利⽤可</button>
				<div class="c-accordion__panel">
					<div class="c-accordion__row">
						<div class="c-accordion__panel-left">会員</div>
						<div class="c-accordion__panel-right">-</div>
					</div>
					<div class="c-accordion__row">
						<div class="c-accordion__panel-left">ビジター</div>
						<div class="c-accordion__panel-right">￥2,000（税抜）／⽉額</div>
					</div>
					<div class="c-accordion__row">
						<div class="c-accordion__panel-left">フリー練習費</div>
						<div class="c-accordion__panel-right">-</div>
					</div>
				</div>
			</div>
		</div>
		<div class="p-index2__content">
			<ul>
				<li>
					<span class="u-hl u-hl--green">レッスン・フリー利用可能時間</span>1時間（準備時間含む、会員・ビジター問わず）
				</li>
				<li>
					<span class="u-hl u-hl--blue">学生割引</span>上記料金より50％OFF（会員カテゴリのみ適用※フリー練習含まず）
				</li>
			</ul>
		</div>
	</div>
</section>
<!--begin section 3-->
<section class="p-index3">
	<div class="p-index3__inner">
		<div class="p-index3__text"><p>見学・体験のご予約もこちらから!</p></div>
		<h3>
			<img src="assets/image/common/text_reservation.png" alt="" width="733" height="51" class="u-pc-only u-mgAuto">
			<img src="assets/image/common/sp/text.png" alt="" width="230" height="67" class="u-sp-only u-mgAuto">
		</h3>
		<div class="p-index3__text2">
			<p>当スクールは予約制となっております。</p>
			<p>お電話、または予約システムよりご予約をお願い致します。</p>
		</div>
		<div class="l-btn l-btn--2center">
			<div class="c-btn2">
				<a href="">
					<img src="assets/image/common/text_btn2.png" alt="" width="182" height="22" class="u-pc-only">
					<img src="assets/image/common/sp/button_text.png" alt="" width="121" height="14" class="u-sp-only">
				</a>
			</div>
			<div class="c-btn2">
				<a href="">
					<img src="assets/image/common/text_btn.png" alt="" width="227" height="45" class="u-pc-only">
					<img src="assets/image/common/sp/button_text2.png" alt="" width="115" height="29" class="u-sp-only">
				</a>
				<div class="p-index3__text3">
					<p>
						平日11:00～21:00（昼休憩13:00～14:00）／<br>
						土・日・祝10:00～19:00 <br>
						定休日 火曜日
					</p>
				</div>
			</div>
		</div>
	</div>
</section>
<!--begin section 4-->
<section class="p-index4">
	<div class="p-index4__inner">
		<div class="p-index4__title">
			<h3>
				<img src="assets/image/common/title_instructor.png" alt="Title" width="735" height="68" class="u-pc-only u-mgAuto">
				<img src="assets/image/common/sp/title_instructor.png" alt="Title" width="289" height="34" class="u-sp-only u-mgAuto">
			</h3>
		</div>
		<div class="p-index4__content">
			<div class="c-imgbox">
				<div class="c-imgbox__image">
					<img src="assets/image/common/avt1.jpg" alt="" width="272" height="204" class="u-pc-only">
					<img src="assets/image/common/sp/avt.jpg" alt="" width="136" height="102" class="u-sp-only u-mgAuto">
				</div>
				<div class="c-imgbox__text">
					<h4><span class="u-pc-only">矢島　嘉彦</span><span class="u-sp-only">講師名が入ります</span></h4>
					<p>1,000人以上のレッスン経験があり、初心者から上級者までそれぞれの悩みや疑問を解決し指導します。</p>
					<div class="c-imgbox__list">
						<ul>
							<li>公益社団法人日本プロゴルフ協会</li>
							<li>高校生からゴルフを始め、2006年にティーチングプロとして社団法人日本プロゴルフ協会の会員となる</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--begin section 5-->
<section class="p-index5">
	<div class="p-index5__inner">
		<div class="p-index5__title">
			<h2>
				<img src="assets/image/common/title_shop.png" alt="Title" width="609" height="68" class="u-pc-only u-mgAuto">
				<img src="assets/image/common/sp/title_shop.png" alt="Title" width="287" height="34" class="u-sp-only u-mgAuto">
			</h2>
		</div>
		<div class="p-index5__content">
			<div class="c-imgoverlay">
				<img src="assets/image/common/imgshop1.jpg" alt="" width="272">
				<div class="c-imgoverlay__text"><p>ダミー画像</p></div>
			</div>
			<div class="c-imgoverlay">
				<img src="assets/image/common/imgshop2.jpg" alt="" width="272">
				<div class="c-imgoverlay__text"><p>ダミー画像</p></div>
			</div>
			<div class="c-imgoverlay">
				<img src="assets/image/common/imgshop3.jpg" alt="" width="272">
				<div class="c-imgoverlay__text"><p>ダミー画像</p></div>
			</div>
			<div class="c-imgoverlay">
				<img src="assets/image/common/imgshop4.jpg" alt="" width="272">
				<div class="c-imgoverlay__text"><p>ダミー画像</p></div>
			</div>
			<div class="c-imgoverlay">
				<img src="assets/image/common/imgshop5.jpg" alt="" width="272">
				<div class="c-imgoverlay__text"><p>ダミー画像</p></div>
			</div>
		</div>
	</div>
</section>
<!--begin section 6-->
<section class="p-index6">
	<div class="p-index6__title">
		<h2>
			<img src="assets/image/common/title_access.png" alt="" width="401" height="48" class="u-pc-only">
			<img src="assets/image/common/sp/title_access.png" alt="" width="201" height="24" class="u-sp-only u-mgAuto">
		</h2>
	</div>
	<div class="p-index6__inner">
		<div class="p-index6__map">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3226.379891317833!2d139.40022691489781!3d36.035433480113156!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6018d5210bdb3f87%3A0xe843e88a64e460b5!2z44Kk44Oz44OJ44Ki44K044Or44OV44K544Kv44O844OrQVJST1dT5p2x5p2-5bGx5bqX!5e0!3m2!1sen!2s!4v1532396378143" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
		<div class="p-index6__info">
			<div class="p-index6__table">
				<table>
					<tr>
						<th>
							<i><img src="assets/image/common/icon1.png" alt="" width="17" height="16"></i> 営業時間
						</th>
						<td><p>平日11:00〜21:00(昼休憩13:00〜14:00)</p>
							<p>土・日・祝10:00〜19:00</p></td>
					</tr>
					<tr>
						<th>
							<i><img src="assets/image/common/icon2.png" alt="" width="17" height="17"></i> 定休日
						</th>
						<td>火曜日</td>
					</tr>
					<tr>
						<th>
							<p><i><img src="assets/image/common/icon3.png" alt="" width="17" height="16"></i> レッスン・フリー</p>
							<p>利用可能時間</p>
						</th>
						<td>会員・ビジター問わず1時間(準備時間含む)</td>
					</tr>
					<tr>
						<th><i><img src="assets/image/common/icon4.png" alt="" width="17" height="14"></i> TEL</th>
						<td>0493-23-8015</td>
					</tr>
					<tr>
						<th><i><img src="assets/image/common/icon5.png" alt="" width="17" height="14"></i> 住所</th>
						<td><p>〒355-0028</p>
							<p>埼玉県東松山市箭弓町1-13-16 安福ビル1F</p></td>
					</tr>
				</table>
			</div>
		</div>

	</div>

</section>
<!--begin section 7-->
<section class="p-index7">
	<div class="p-index7__inner">
		<div class="p-index7__title">
			<h3>
				<img src="assets/image/common/title_info.png" alt="" width="310" height="96" class="u-pc-only u-mgAuto">
				<img src="assets/image/common/sp/title_info.png" alt="" width="171" height="52" class="u-sp-only u-mgAuto">
			</h3>
		</div>
		<div class="p-index7__text">
			<p><span class="u-breakline-sp">2018年0月0日</span>　インドアゴルフスクール ARROWS 東松山店 サイト公開しました。</p>
		</div>
	</div>
</section>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>